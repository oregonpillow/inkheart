#!/bin/bash

if [ "$VITE_ENV" = "dev" ]; then
  #if dev don't waste time building
  # If using firebase, make sure dev server uses auth
  if [ -n "$FIREBASE_PROJECT_ID" ]; then
    sed -i 's/\$INK:USEAUTH/true/g' index.html
  fi
else
  npm run build
fi