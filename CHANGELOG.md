# Changelog

All notable changes to this project will be documented in this file.

## [0.8.3] - Crispin (2024-12-31)

### Added

- Collapsible sidebar


## [0.7.0] - Horatio (2024-01-29)

### Added

- Sorting functionality for name, modified, and created fields.
- Simple search bar.
- Various UI improvements.
- Dark Mode Toggle
- Use of codenames for versions because why not? It's fun.

### Changed

- Replaced [tinro](https://github.com/AlexxNB/tinro) with [Svelte-Navigator](https://github.com/Nystik-gh/svelte-navigator) for routing. _(Using a custom fork to solve dependency issues)._
- Refactored versioning into a single source of truth.

### Updated

- Upgraded to Svelte 4 and upgraded dependencies accordingly.

### Fixed

- Issue in routing related to a [bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1742396) in Firefox.
- Various minor issues.
