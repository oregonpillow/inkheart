<section>
  <p align="center">
      <img src="frontend/public/static/logo.png" alt="Inkheart logo" width="200" height="200">
  </p>

  <h3 align="center">Inkheart</h3>

  <p align="center">
    A self-hosted PDF library indexer and reader.
  </p>
</section>
<br><br>

<p align="center">
    <img src="screenshots/example.png" alt="example screenshot">
</p>

## Built with

<p align="center">
  <a href="https://www.rust-lang.org/">
    <img src="https://cdn.worldvectorlogo.com/logos/rust.svg" alt="Rust" width="41">
  </a>
  <a href="https://svelte.dev/">
    <img src="https://cdn.worldvectorlogo.com/logos/svelte-1.svg" alt="Svelte" width="35">
  </a>
  <a href="https://www.docker.com/">
    <img src="https://cdn.worldvectorlogo.com/logos/docker-4.svg" alt="Docker" width="41">
  </a>
  <a href="https://mozilla.github.io/pdf.js/">
    <img src="https://mozilla.github.io/pdf.js/images/logo.svg" alt="Docker" width="41">
  </a>
</p>

## Features

- Embedded PDF.js reader
- Direct linking to specific pages
- Optional authentication via Firebase Auth with whitelisting
- Light and dark color themes

## Limitations

- Does not store metadata
- Index is stored in-memory and may scale poorly for large libraries
- Search pulls the entire index and filters on the client, scales poorly for large libraries.

## Why does this exist?

Considering the amount of other self-hosted PDF/E-book applications with more features (Paperless-ngx, Kavita, Komga, etc), why make this? Aside from it being a fun project, I also felt most of the existing applications were overkill for my needs, I mainly need to be able to link to a page in a PDF from Notion.so, and I have some PDFs that are too large to be viewed by Google Drive's built in reader. So having a light weight, simple, hosting solution was all I needed.

## Setup with Docker Compose

You can pull the latest image from docker hub: https://hub.docker.com/r/nobbe/inkheart .
Ensure that volume mounts already exist: e.g. `mkdir /path/to/config /path/to/covers /path/to/thumbnails /path/to/media` before running container to prevent permission issues.

example docker-compose.yml:

```yaml
services:
  inkheart:
    image: inkheart:latest
    container_name: inkheart
    network_mode: bridge
    ports:
      - 8080:8080/tcp
    volumes:
      - /path/to/media:/media # The path where you're storing your PDF files
      - /path/to/covers:/covers # This is where the first page of each file will be extracted to to serve as a thumbnail cover
      - /path/to/config:/config # Config folder, e.g for whitelist file
      - /path/to/thumbnails:/thumbnails # This folder will contain thumbnails generated for OpenGraph preview of pages when linking.
    environment:
      - MEDIA_DIR=/media
      - IMAGE_DIR=/covers
      - THUMBNAIL_DIR=/thumbnails
      - CONFIG_PATH=/config/inkheart.toml # path to a config file
      - BIND_ADDR=0.0.0.0
      - BIND_PORT=8080
      - SCAN_INTERVAL=600 # Scan interval in seconds
      - FIREBASE_PROJECT_ID=<ID_HERE> # Optional firebase project id if you want to use firebase authentication
      - FIREBASE_WHITELIST=/config/whitelist.cfg # whitelist of account ids from firebase. /config
    restart: always
```

## Config

The configuration is cumulative, meaning that values from different configuration sources are merged. Higher-priority sources take precedence over values from previous sources. The supported configuration sources are:

1. **Environment Variables**

   - Hardcoded defaults are applied for variables not defined in the environment.

2. **Config File**
   - Overrides environment config
   - TOML format

> **_NOTE:_** The variable `CONFIG_PATH` is only read from the environment and can be used to set the location of your config file.

### Variables

| Variable                | Description                                                                                    | Default Value       |
| ----------------------- | ---------------------------------------------------------------------------------------------- | ------------------- |
| **CONFIG_PATH**         | Path to configuration file. _Only loaded from environment, ignored when read from config file_ | `"./inkheart.toml"` |
| **MEDIA_DIR**           | Directory for media files.                                                                     | `"/media"`          |
| **IMAGE_DIR**           | Directory for cover images.                                                                    | `"/covers"`         |
| **THUMBNAIL_DIR**       | Directory for thumbnails.                                                                      | `"/thumbnails"`     |
| **BIND_ADDR**           | Address to bind the server to.                                                                 | `"0.0.0.0"`         |
| **BIND_PORT**           | Port to bind the server to.                                                                    | `"8080"`            |
| **SCAN_INTERVAL**       | Scan interval for re-indexing (in seconds).                                                    | `"600"`             |
| **FIREBASE_PROJECT_ID** | Firebase project ID for authentication.                                                        | none (optional)     |
| **FIREBASE_WHITELIST**  | File path for the whitelist of account IDs from Firebase.                                      | none (optional)     |

## Indexing

The application scans the given media directory immediately on startup and then schedules re-indexing with the interval specified. The application will display files in the same directory structure as in the media directory.

### Covers and Thumbnails directories

The application extracts the first page of each file found in the media directory to use as a high-res cover thumbnail. This extraction is done when a file is first indexed. As for thumbnails, these are extracted on demand as a link specifying a page number is either visited or linked to from a site that supports OpenGraph image previews. Thumbnails are extracted at a lower resolution.

> **_NOTE:_** Files are indexed based on their path. If a file is moved to a different folder, or renamed, the indexing hash will not match and the cover will be extracted again.

## Authentication

### Firebase Authentication

Create a Firebase project and enable the Authentication provider(s) of your choice (Sign-in with google, email, etc). Specify project id in the `FIREBASE_PROJECT_ID` config variable. The application should now require sign-in.

### Firebase Whitelist

After signing in once you can open the Firebase Console and find the ID of the user(s) you'd like to whitelist rather than allow anyone with a Google account for example. The whitelist file should have one id per line, and the path to the whitelist is specified in the `FIREBASE_WHITELIST` config variable. After a restart only whitelisted users should be able to sign in.

## Screenshots

<img src="screenshots/folders.png" align="center" width="25%" alt="Folders">
<img src="screenshots/files.png" align="center" width="25%" alt="Files">
<img src="screenshots/reader.png" align="center" width="25%" alt="Reader">
<img src="screenshots/opengraph-preview.png" align="center" width="25%" alt="Link Preview">

## Future plans (if I feel like it)

- set up build procedure for running without Docker
- ~~add search~~ Done!
- ~~dynamic OpenGraph image preview, using the actual page linked instead of cover~~ Done!
- add support for custom collections or favourites
