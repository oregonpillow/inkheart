#!/bin/bash

# Default values
credentials="user:password"
basicauth=false
firebase=false

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        --basicauth)
            basicauth=true
            shift
            ;;
        --firebase)
            firebase=true
            shift
            ;;
        --credentials)
            credentials="$2"
            shift 2
            ;;
        *)
            echo "Unknown parameter: $1"
            exit 1
            ;;
    esac
done

# Validate exclusive switches
if [ "$basicauth" = true ] && [ "$firebase" = true ]; then
    echo "--basicauth and --firebase are exclusive switches. Please provide only one of them."
    exit 1
fi

docker_compose_command="docker compose -f dev/docker-compose.dev.yml"

if [ "$basicauth" = true ]; then
    # Split credentials into username and password
    IFS=':' read -r username password <<< "$credentials"

    # Compute SHA1 hash and convert to base64
    hashed_passwd=$(echo -n "$password" | sha1sum | cut -d' ' -f1 | xxd -r -p | base64)

    # Create htpasswd entry
    htpasswd_entry="${username}:{SHA}${hashed_passwd}"
    htpasswd_file="dev/nginx/.htpasswd"

    # Write to htpasswd file
    echo "$htpasswd_entry" > "$htpasswd_file"

    # Use basic auth env
    docker_compose_command+=" --env-file dev/.env.basicauth"
fi

if [ "$firebase" = true ]; then
    # Use firebase env
    docker_compose_command+=" --env-file dev/.env.firebase"
fi

docker_compose_command+=" watch"

# Execute the command
eval "$docker_compose_command"