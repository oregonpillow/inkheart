param(
    [switch]$basicauth,
    [switch]$firebase,
    [string]$credentials = "user:password"
)

# Validate exclusive switches
if ($basicauth -and $firebase) {
    Write-Host "-basicauth and -firebase are exclusive switches. Please provide only one of them."
    exit
}

$dockerComposeCommand = "docker compose -f dev/docker-compose.dev.yml"

if ($basicauth) {
    $username, $password = $credentials -split ":", 2

    # Compute hash over password
    $passwordBytes = [System.Text.Encoding]::ASCII.GetBytes($password)
    $sha1 = [System.Security.Cryptography.SHA1]::Create()
    $hash = $sha1.ComputeHash($passwordBytes)

    $hashedpasswd = [convert]::ToBase64String($hash)

    $htpasswdEntry = "${username}:{SHA}${hashedpasswd}"
    $htpasswdFile = "dev/nginx/.htpasswd"

    $htpasswdEntry | Out-File -FilePath $htpasswdFile -Encoding ASCII

    # use basic auth env
    $dockerComposeCommand += " --env-file dev/.env.basicauth"
}

if ($firebase) {
    # use firebase env
    $dockerComposeCommand += " --env-file dev/.env.firebase"
}

$dockerComposeCommand += " watch"

Invoke-Expression $dockerComposeCommand