worker_processes 2;
user www-data;

events {
    use epoll;
    worker_connections 128;
}

http {
    server_tokens off;
    include mime.types;
    charset utf-8;

    server {
        listen 80;
        server_name localhost;

        location / {
            auth_basic "Restricted Access";
            auth_basic_user_file /etc/nginx/.htpasswd;

            proxy_pass http://frontend:5173;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;

            # WebSocket support
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }

        location ~ ^/(mediaroot|file|item|cover|thumbnail|search) {
            auth_basic "Restricted Access";
            auth_basic_user_file /etc/nginx/.htpasswd;

            rewrite ^/(mediaroot|file|item|cover|thumbnail|search)(.*)$ /$1$2 break;
            proxy_pass http://inkheart:8080;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
            root /usr/share/nginx/html;
        }
    }
}