use std::{
    env::current_dir,
    path::{Path, PathBuf},
    sync::Arc,
};

use pdfium_render::pdfium::Pdfium;

pub struct BindingsWrapper(pub Pdfium);

unsafe impl Send for BindingsWrapper {}
unsafe impl Sync for BindingsWrapper {}

pub fn init_pdfium() -> Arc<BindingsWrapper> {
    let file_name = Pdfium::pdfium_platform_library_name();

    let lib_dir = Path::new("./lib");
    let current_dir = current_dir().unwrap_or(PathBuf::from("./"));

    let lib_path = match current_dir.join(&file_name).exists() {
        true => current_dir.join(&file_name),
        false => match lib_dir.join(&file_name).exists() {
            true => lib_dir.join(&file_name),
            false => panic!(
                r#"Failed to bind to Pdfium Library! File "{}" not found in either working directory or ./lib"#,
                &file_name.to_string_lossy().to_owned()
            ),
        },
    };

    let bindings = match Pdfium::bind_to_library(&lib_path) {
        Ok(b) => b,
        Err(error) => panic!("Failed to load Pdfium library! {}", error),
    };

    Arc::new(BindingsWrapper(Pdfium::new(bindings)))
}
