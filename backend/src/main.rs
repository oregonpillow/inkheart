mod config;
mod indexer;
mod pdfium;
mod server;

use std::{
    path::Path,
    sync::{Arc, Mutex},
};

use config::Config;
use firebase_admin_auth_rs::jwk_auth::JwkAuth;

use indexer::{start_scheduled_indexer, LookupMap};
use server::{start_server, state::AppData};

use crate::pdfium::bindings::init_pdfium;

/* import version data from build script artifact */
/* literally just testing cache hit or not */
include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Inkheart v{} ({})", VERSION, CODENAME);

    let config = Config::load_config();
    let pdfium = init_pdfium();

    let auth = match config.firebase_project_id.clone() {
        Some(project_id) => Some(JwkAuth::new(project_id).await),
        None => None,
    };

    let state = Arc::new(Mutex::new(AppData {
        lookup_table: LookupMap::from_dir(&Path::new(&config.media_root.clone())),
        config,
        auth,
        pdfium,
    }));

    match start_scheduled_indexer(&state) {
        Ok(_) => (),
        Err(e) => println!("Failed to start scan scheduler: {}", e.to_string()),
    };

    start_server(&state).await
}
