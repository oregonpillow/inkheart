use std::{
    collections::HashMap,
    fs,
    path::Path,
    time::{Duration, UNIX_EPOCH},
};

use serde::Serialize;

use super::utils::hash_string;

//implements serialize so it can be sent as json response
#[derive(Clone, Serialize, Debug)]
pub struct ItemRecord {
    pub id: String,
    pub filename: String,
    pub title: String,
    pub cover_name: String,
    pub file_path: String,
    pub is_dir: bool,
    pub dir_previews: Vec<String>,
    pub children: Vec<SafeItemRecord>,
    pub last_modified: u64,
    pub created: u64,
}

//implements serialize so it can be sent as json response
#[derive(Clone, Serialize, Debug)]
pub struct SafeItemRecord {
    pub id: String,
    pub filename: String,
    pub title: String,
    pub cover_name: String,
    pub is_dir: bool,
    pub dir_previews: Vec<String>,
    pub children: Vec<SafeItemRecord>,
    pub last_modified: u64,
    pub created: u64,
}

impl From<ItemRecord> for SafeItemRecord {
    fn from(item: ItemRecord) -> Self {
        let ItemRecord {
            id,
            filename,
            title,
            cover_name,
            file_path: _,
            is_dir,
            dir_previews,
            children,
            last_modified,
            created,
        } = item;

        SafeItemRecord {
            id,
            filename,
            title,
            cover_name,
            is_dir,
            dir_previews,
            children,
            last_modified,
            created,
        }
    }
}

impl From<&ItemRecord> for SafeItemRecord {
    fn from(item: &ItemRecord) -> Self {
        let ItemRecord {
            id,
            filename,
            title,
            cover_name,
            file_path: _,
            is_dir,
            dir_previews,
            children,
            last_modified,
            created,
        } = item.clone();

        SafeItemRecord {
            id,
            filename,
            title,
            cover_name,
            is_dir,
            dir_previews,
            children,
            last_modified,
            created,
        }
    }
}

impl ItemRecord {
    pub fn from_path(path: &Path) -> ItemRecord {
        let file_path = path.to_string_lossy().to_string();
        let hash = hash_string(&file_path);
        let title = path
            .file_stem()
            .map(|s| s.to_string_lossy().to_string())
            .unwrap_or("".to_string());
        let filename = path
            .file_name()
            .map(|s| s.to_string_lossy().to_string())
            .unwrap_or("".to_string());

        let cover_file = format!("{}-{}.jpg", hash, title);

        let cover_name = cover_file;

        let (last_modified, created) = {
            let meta = fs::metadata(&file_path);
            match meta {
                Ok(meta) => {
                    let last_modified: u64 = meta
                        .modified()
                        .map(|st| {
                            st.duration_since(UNIX_EPOCH)
                                .unwrap_or(Duration::from_secs(0))
                                .as_secs()
                        })
                        .unwrap_or(0);
                    let created = meta
                        .created()
                        .map(|st| {
                            st.duration_since(UNIX_EPOCH)
                                .unwrap_or(Duration::from_secs(0))
                                .as_secs()
                        })
                        .unwrap_or(0);
                    (last_modified, created)
                }
                Err(_) => (0, 0),
            }
        };

        ItemRecord {
            id: hash,
            filename,
            title,
            cover_name,
            file_path,
            is_dir: path.is_dir(),
            dir_previews: vec![],
            children: vec![],
            last_modified,
            created,
        }
    }
}

//implements serialize so it can be sent as json response
#[derive(Clone, Serialize)]
pub struct LookupMap(HashMap<String, ItemRecord>);

impl LookupMap {
    pub fn from_dir(dir: &Path) -> LookupMap {
        let mut map: HashMap<String, ItemRecord> = HashMap::new();
        build_map(dir, &mut map);

        LookupMap(map)
    }

    pub fn get_map(&mut self) -> &mut HashMap<String, ItemRecord> {
        &mut self.0
    }
}

fn build_map(dir: &Path, mut map: &mut HashMap<String, ItemRecord>) -> () {
    if dir.is_dir() {
        let mut dir_record = ItemRecord::from_path(dir);
        if let Ok(entries) = fs::read_dir(dir) {
            for entry in entries {
                if let Ok(entry) = entry {
                    let path = entry.path();
                    if path.is_dir() {
                        let record = ItemRecord::from_path(&path);
                        dir_record
                            .children
                            .push(SafeItemRecord::from(record.clone()));
                        build_map(&path, &mut map);
                    } else {
                        if let Some(extension) = path.extension() {
                            if extension.to_ascii_lowercase().to_string_lossy().to_string() == "pdf"
                            {
                                let record = ItemRecord::from_path(&path);
                                dir_record
                                    .children
                                    .push(SafeItemRecord::from(record.clone()));
                                map.insert(record.id.clone(), record);
                            }
                        }
                    }
                }
            }
        }
        map.insert(dir_record.id.clone(), dir_record);
    }
}
