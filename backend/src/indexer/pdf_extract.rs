use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

use pdfium_render::prelude::*;

use crate::pdfium::bindings::BindingsWrapper;

pub fn extract_pdf_cover(
    cover_path: &String,
    cover_name: &String,
    file_path: &String,
    pdfium: Arc<BindingsWrapper>,
) -> Result<(), String> {
    let cover_file = std::path::Path::new(cover_path).join(cover_name);

    extract_pdf_page(file_path, &cover_file, 0, 900, 2000, pdfium)?;

    Ok(())
}

pub async fn extract_pdf_thumbnail(
    thumbnail_path: &String,
    id: &String,
    page_index: u16,
    file_path: &String,
    pdfium: Arc<BindingsWrapper>,
) -> Result<String, String> {
    // Check if the thumbnail image already exists in the root dir for covers
    let filename = format!("{id}-{page_index}.jpg");
    let cover_file = std::path::Path::new(thumbnail_path).join(&filename);
    println!("Extracting thumbnail");

    extract_pdf_page(file_path, &cover_file, page_index, 300, 600, pdfium)?;

    Ok(filename)
}

fn extract_pdf_page(
    file_path: &String,
    output: &PathBuf,
    page_index: u16,
    width: i32,
    height: i32,
    pdfium: Arc<BindingsWrapper>,
) -> Result<(), String> {
    // Check if the cover image already exists in the root dir for covers
    if output.exists() {
        return Ok(());
    }

    println!("Extracting page from {}", file_path);

    let document = pdfium
        .0
        .load_pdf_from_file(Path::new(file_path), None)
        .map_err(|e| e.to_string())?;

    // ... set rendering options that will be applied to all pages...

    let render_config = PdfRenderConfig::new()
        .set_target_width(width)
        .set_maximum_height(height)
        .rotate_if_landscape(PdfPageRenderRotation::Degrees90, true);

    let page = document
        .pages()
        .get(page_index)
        .map_err(|e| e.to_string())?;

    page.render_with_config(&render_config)
        .map_err(|e| e.to_string())?
        .as_image() // Renders this page to an image::DynamicImage...
        .as_rgba8() // ... then converts it to an image::Image...
        .ok_or(PdfiumError::ImageError)
        .map_err(|e| e.to_string())?
        .save_with_format(output, image::ImageFormat::Jpeg) // ... and saves it to a file.
        .map_err(|e| e.to_string())?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs;

    use crate::pdfium::bindings::init_pdfium;

    use super::*;

    #[test]
    fn pdfium_extract() {
        let pdfium = init_pdfium();
        let result = extract_pdf_cover(
            &"./test".to_string(),
            &"sample.jpg".to_string(),
            &"./test/sample.pdf".to_string(),
            pdfium,
        );
        assert!(result.is_ok(), "Failed to extract pdf cover! {:?}", result);
        fs::remove_file(Path::new("./test/sample.jpg")).ok();
    }
}
