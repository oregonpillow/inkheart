use std::{
    path::Path,
    sync::{Arc, Mutex},
};

use crate::{
    indexer::{pdf_extract::extract_pdf_cover, LookupMap},
    server::state::AppData,
};

pub fn scan_library(state: Arc<Mutex<AppData>>) -> Result<(), String> {
    let mut lookup = None;
    let mut cover_root = String::new();
    let mut pdfium = None;
    println!("Running indexer...");
    match state.lock() {
        Ok(mut data) => {
            println!("Update lookup table...");
            let lookup_table = LookupMap::from_dir(Path::new(&data.config.media_root));
            lookup = Some(lookup_table.clone());
            cover_root = data.config.cover_root.clone();

            data.lookup_table = lookup_table;

            pdfium = Some(data.pdfium.clone());

            drop(data);
            println!("Lookup table updated...");
        }
        Err(e) => println!("{}", e),
    }

    if let Some(mut lookup) = lookup {
        for (_, item) in lookup.get_map() {
            if !item.is_dir && pdfium.is_some() {
                extract_pdf_cover(
                    &cover_root,
                    &item.cover_name,
                    &item.file_path,
                    pdfium.clone().unwrap(),
                )?;
            }
        }
    }
    println!("Indexer finished.");
    Ok(())
}
