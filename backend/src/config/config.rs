use config_file::{Config as ConfigFile, ConfigError, File, Source};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, env, path::Path};

const CONFIG_FILE_PATH: &str = "./inkheart.toml";

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Config {
    pub media_root: String,
    pub cover_root: String,
    pub thumbnail_root: String,
    pub config_path: String,
    pub bind_addr: String,
    pub bind_port: u16,
    pub scan_interval: u64,
    pub firebase_project_id: Option<String>,
    pub whitelist: Option<String>,
    pub clacks_overhead_msg: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ConfigToml {
    #[serde(alias = "MEDIA_DIR")]
    pub media_root: Option<String>,
    #[serde(alias = "IMAGE_DIR")]
    pub cover_root: Option<String>,
    #[serde(alias = "THUMBNAIL_DIR")]
    pub thumbnail_root: Option<String>,
    #[serde(alias = "CONFIG_PATH")]
    pub config_path: Option<String>,
    #[serde(alias = "BIND_ADDR")]
    pub bind_addr: Option<String>,
    #[serde(alias = "BIND_PORT")]
    pub bind_port: Option<u16>,
    #[serde(alias = "SCAN_INTERVAL")]
    pub scan_interval: Option<u64>,
    #[serde(alias = "FIREBASE_PROJECT_ID")]
    pub firebase_project_id: Option<String>,
    #[serde(alias = "FIREBASE_WHITELIST")]
    pub whitelist: Option<String>,
    #[serde(alias = "CLACKS_OVERHEAD")]
    pub clacks_overhead_msg: Option<String>,
}

impl Source for Config {
    fn clone_into_box(&self) -> Box<dyn Source + Send + Sync> {
        Box::new(self.clone())
    }

    fn collect(&self) -> Result<config_file::Map<String, config_file::Value>, ConfigError> {
        let s = serde_json::to_string(self).map_err(|e| ConfigError::Message(e.to_string()))?;
        let map: HashMap<String, config_file::Value> =
            serde_json::from_str(&s).map_err(|e| ConfigError::Message(e.to_string()))?;

        Ok(map)
    }
}

impl Config {
    pub fn from_env() -> Config {
        println!("Loading from environment...");

        let config_path = env::var_os("CONFIG_PATH")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or(CONFIG_FILE_PATH.to_string());
        let media_root = env::var_os("MEDIA_DIR")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or("./media".to_string());
        let cover_root = env::var_os("IMAGE_DIR")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or("./covers".to_string());
        let thumbnail_root = env::var_os("THUMBNAIL_DIR")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or("./covers".to_string());
        let bind_addr = env::var_os("BIND_ADDR")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or("127.0.0.1".to_string());
        let bind_port = env::var_os("BIND_PORT")
            .and_then(|s| s.to_string_lossy().parse().ok())
            .unwrap_or(8080);
        let scan_interval = env::var_os("SCAN_INTERVAL")
            .and_then(|s| s.to_string_lossy().parse().ok())
            .unwrap_or(600);
        let firebase_project_id = env::var_os("FIREBASE_PROJECT_ID")
            .map(|s| s.to_string_lossy().into_owned())
            .filter(|s| !s.is_empty());
        let whitelist = env::var_os("FIREBASE_WHITELIST")
            .map(|s| s.to_string_lossy().into_owned())
            .filter(|s| !s.is_empty());

        let clacks_overhead_msg = env::var_os("CLACKS_OVERHEAD")
            .map(|s| s.to_string_lossy().into_owned())
            .unwrap_or("GNU Terry Pratchet".to_string());

        Config {
            config_path,
            media_root,
            cover_root,
            thumbnail_root,
            bind_addr,
            bind_port,
            scan_interval,
            firebase_project_id,
            whitelist,
            clacks_overhead_msg,
        }
    }

    pub fn load_config() -> Config {
        println!("Loading Config...");
        let base = Config::from_env();
        if !Path::new(&base.config_path).exists() {
            println!(
                "path not found: {}, falling back to environment",
                CONFIG_FILE_PATH
            );
            return base;
        }

        println!("Loading from {}", &base.config_path);

        // there has to be a better way of doing this but for now this is how we're doing it.
        let conf = (|| {
            // build optional version of config from config file
            let conf = ConfigFile::builder()
                .add_source(File::with_name(&base.config_path).required(false))
                .build()
                .and_then(|c| c.try_deserialize::<ConfigToml>())?;

            // convert struct to HashMap using serde, I'm not sorry
            let s =
                serde_json::to_string(&conf).map_err(|e| ConfigError::Message(e.to_string()))?;
            let override_map: HashMap<String, Option<config_file::Value>> =
                serde_json::from_str(&s).map_err(|e| ConfigError::Message(e.to_string()))?;

            // Build config again using our optional config as overrides.
            // Because if we're being dumb why not go all in?
            let mut merged_builder = ConfigFile::builder().add_source(base.clone());

            for (key, value) in &override_map {
                merged_builder = merged_builder
                    .set_override_option::<&String, config_file::Value>(key, value.clone())?;
            }

            merged_builder
                .build()
                .and_then(|c| c.try_deserialize::<Config>())
        })();

        match conf {
            Ok(conf) => conf,
            Err(e) => {
                println!(
                    "config error: {}, falling back to environment",
                    e.to_string()
                );
                base
            }
        }
    }
}
