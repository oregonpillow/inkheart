use std::{
    io,
    sync::{Arc, Mutex},
};

use firebase_admin_auth_rs::jwk_auth::JwkAuth;

use crate::{config::Config, indexer::LookupMap, pdfium::bindings::BindingsWrapper};

pub struct AppData {
    pub lookup_table: LookupMap,
    pub config: Config,
    pub auth: Option<JwkAuth>,
    pub pdfium: Arc<BindingsWrapper>,
}

pub fn get_config(state: &Arc<Mutex<AppData>>) -> io::Result<Config> {
    let data = state.clone();

    let data = data.lock().map_err(|_e| io::ErrorKind::Other)?;

    Ok(data.config.clone())
}

pub fn get_pdfium(state: &Arc<Mutex<AppData>>) -> io::Result<Arc<BindingsWrapper>> {
    let data = state.clone();

    let data = data.lock().map_err(|_e| io::ErrorKind::Other)?;

    Ok(data.pdfium.clone())
}
