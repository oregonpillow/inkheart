use std::{
    fs, io,
    sync::{Arc, Mutex},
};

use actix_web::{http::StatusCode, web, HttpResponse, Responder};

use crate::server::state::{get_config, AppData};

pub async fn index(data: web::Data<Arc<Mutex<AppData>>>) -> io::Result<impl Responder> {
    let html = fs::read_to_string("./public/index.html")?;

    let config = get_config(&data)?;

    let title = "Inkheart".to_string();
    let img = "/static/inkheart.png".to_string();

    let use_auth = match config.firebase_project_id.is_some() {
        true => "true",
        false => "false",
    };

    let hydrated = html
        .replace("$OGTITLE", &title)
        .replace("$OGIMAGE", &img)
        .replace("$INK:USEAUTH", &use_auth);

    // response
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .insert_header(("X-Clacks-Overhead", config.clacks_overhead_msg.clone()))
        .body(hydrated))
}
