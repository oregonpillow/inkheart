use std::{
    cmp,
    fs::{self},
    io,
    path::Path,
    sync::{Arc, Mutex},
};

use actix_web::{http::StatusCode, routes, web, HttpResponse, Responder};
use serde::Deserialize;

use crate::{
    indexer::{extract_pdf_thumbnail, LookupMap},
    server::state::{get_config, get_pdfium, AppData},
};

#[derive(Deserialize)]
pub struct Info {
    preview: Option<String>,
    page: Option<u16>,
}

// TODO: add setting to make this a secure route
// this route deliberately leaks the title and thumbnail paths
// in order for OpenGraph previews to work
// future improvment would use a setting to block this behaviour
#[routes]
#[get("/reader/{id}")]
#[get("/fullscreen/{id}")]
#[get("/embed/{id}")]
pub async fn multihandler(
    data: web::Data<Arc<Mutex<AppData>>>,
    path: web::Path<String>,
    info: web::Query<Info>,
) -> io::Result<impl Responder> {
    let id = path.into_inner();
    let html = fs::read_to_string("./public/index.html")?;

    //let data = data.clone();

    let config = get_config(&data)?;
    let pdfium = get_pdfium(&data)?;

    let mut lookup = None;
    if let Ok(data) = data.lock() {
        let lookup_table = LookupMap::from_dir(Path::new(&data.config.media_root));
        lookup = Some(lookup_table.clone());

        drop(data);
    }

    let mut title = String::new();
    let mut img = String::new();

    if let Some(mut lookup) = lookup {
        let record = lookup.get_map().get(&id).ok_or(io::ErrorKind::NotFound)?;
        title = record.title.clone();

        //let cover_file = std::path::Path::new(&thumbnail_root).join(&record.cover_name);

        let page = info.page.map(|n| cmp::max(n - 1, 0)).unwrap_or(0);

        let filename = extract_pdf_thumbnail(
            &config.thumbnail_root.clone(),
            &id,
            page,
            &record.file_path,
            pdfium,
        )
        .await
        .ok();

        img = match filename {
            Some(f) => {
                format!("/thumbnail/{}", f)
            }
            None => "/static/inkheart.png".to_string(),
        }
    }

    if let Some(preview) = info.preview.clone() {
        title = preview
    }

    let use_auth = match config.firebase_project_id.is_some() {
        true => "true",
        false => "false",
    };

    let hydrated = html
        .replace("$OGTITLE", &title)
        .replace("$OGIMAGE", &img)
        .replace("$INK:USEAUTH", &use_auth);

    // response
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .insert_header(("X-Clacks-Overhead", config.clacks_overhead_msg.clone()))
        .body(hydrated))
}
