use std::{
    io,
    sync::{Arc, Mutex},
};

use actix_web::{get, web, Responder};

use crate::{
    indexer::{utils::hash_string, SafeItemRecord},
    server::{
        auth::FirebaseUser,
        state::{get_config, AppData},
    },
};

#[get("/search")]
pub async fn search(
    data: web::Data<Arc<Mutex<AppData>>>,
    _user: FirebaseUser,
) -> io::Result<impl Responder> {
    let config = get_config(&data)?;
    let root_id = hash_string(&config.media_root);

    let data = data.clone();
    let mut data = data.lock().map_err(|_e| io::ErrorKind::Other)?;

    let mut map_copy = data.lookup_table.get_map().clone();
    let removed = map_copy.remove(&root_id);

    println!("removed {:?}", removed);

    let safe_vec = map_copy
        .values()
        .map(|record| {
            let mut safe: SafeItemRecord = record.into();
            safe.children.clear();
            safe
        })
        .collect::<Vec<SafeItemRecord>>();

    Ok(web::Json(safe_vec))
}
