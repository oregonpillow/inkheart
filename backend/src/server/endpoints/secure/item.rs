use std::{
    io,
    sync::{Arc, Mutex},
};

use actix_web::{get, web, Responder};

use crate::{
    indexer::SafeItemRecord,
    server::{auth::FirebaseUser, state::AppData},
};

#[get("/item/{id}")]
pub async fn item(
    data: web::Data<Arc<Mutex<AppData>>>,
    path: web::Path<String>,
    _user: FirebaseUser,
) -> io::Result<impl Responder> {
    let id = path.into_inner();
    library_path(data, id, _user).await
}

pub async fn library_path(
    data: web::Data<Arc<Mutex<AppData>>>,
    id: String,
    _user: FirebaseUser,
) -> io::Result<impl Responder> {
    let data = data.clone();
    let mut data = data.lock().map_err(|_e| io::ErrorKind::Other)?;

    let record = data
        .lookup_table
        .get_map()
        .get(&id)
        .ok_or(io::ErrorKind::NotFound)?;

    Ok(web::Json(SafeItemRecord::from(record.clone())))
}
