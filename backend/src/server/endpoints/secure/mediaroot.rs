use std::{
    io,
    sync::{Arc, Mutex},
};

use actix_web::{get, web, Responder};

use crate::{
    indexer::utils::hash_string,
    server::{
        auth::FirebaseUser,
        endpoints::library_path,
        state::{get_config, AppData},
    },
};

#[get("/mediaroot")]
pub async fn mediaroot(
    data: web::Data<Arc<Mutex<AppData>>>,
    _user: FirebaseUser,
) -> io::Result<impl Responder> {
    let config = get_config(&data)?;
    let id = hash_string(&config.media_root);

    library_path(data, id, _user).await
}
