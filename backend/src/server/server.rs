use std::sync::{Arc, Mutex};

use actix_web::{web, App, HttpServer};

use crate::server::{
    endpoints::{file, image, index, item, mediaroot, multihandler, search, thumbnail},
    state::get_config,
};

use super::state::AppData;

pub async fn start_server(state: &Arc<Mutex<AppData>>) -> std::io::Result<()> {
    println!("Server started");

    let config = get_config(state)?;

    let state = state.clone();

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(state.clone()))
            .service(mediaroot)
            .service(item)
            .service(file)
            .service(search)
            .service(image)
            .service(thumbnail)
            .service(multihandler)
            .service(
                actix_files::Files::new("/static", "./public/static")
                    .default_handler(web::get().to(index)),
            )
            .route("/", web::get().to(index))
            .default_service(web::get().to(index))
    })
    .bind((config.bind_addr.clone(), config.bind_port.clone()))?
    .run()
    .await
}
