import { get } from 'svelte/store'
import { config } from '../config'
import { usingAuth } from '../stores/auth'
import { getToken } from './localstorage'

const createRequest = (
  endpoint: string,
  method: string,
  headers?: { [key: string]: string }
): [string, RequestInit] => {
  let { api_base_url } = config

  const authHeader = get(usingAuth) ? { Authorization: 'Bearer ' + getToken() } : {}
  const headerObject = headers ? { ...headers, ...authHeader } : authHeader

  const options =
    Object.keys(headerObject).length > 0
      ? { method, headers: new Headers({ ...headerObject, ...authHeader }) }
      : { method }

  const slashPrefix = endpoint.startsWith('/') ? '' : '/'
  const resource = `${api_base_url}${slashPrefix}${endpoint}`

  return [resource, options]
}

const createGetRequest = (endpoint: string, headers?: { [key: string]: string }): [string, RequestInit] => {
  return createRequest(endpoint, 'get', headers)
}

export { createRequest, createGetRequest }
