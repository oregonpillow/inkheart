import { get } from 'svelte/store'
import type { Theme } from '../stores/options'
import { selectedTheme } from '../stores/options'

const initTheme = () => {
  let selected = get(selectedTheme)

  setThemeOverride(selected)

  selectedTheme.subscribe((theme) => {
    setThemeOverride(theme)
  })
}

const setThemeOverride = (theme: Theme) => {
  const root = document.querySelector('html')
  if (theme === 'system') {
    root.classList.remove('light', 'dark')
  } else if (theme === 'dark') {
    root.classList.remove('light')
    root.classList.add('dark')
  } else if (theme === 'light') {
    root.classList.remove('dark')
    root.classList.add('light')
  }
}

const getSystemTheme = (): 'dark' | 'light' => {
  let systemTheme: Theme =
    window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'

  return systemTheme
}

const getActiveTheme = (): 'light' | 'dark' => {
  let systemTheme = getSystemTheme()
  let selected: Theme = get(selectedTheme)

  let active = selected === 'system' ? systemTheme : selected

  return active
}

const updateTheme = (theme: Theme) => {
  let systemTheme = getSystemTheme()

  let themeToSet = theme === systemTheme ? 'system' : theme

  selectedTheme.set(themeToSet)
}

export { initTheme, getActiveTheme, updateTheme }
