// Import the functions you need from the SDKs you need
import { memoize } from 'lodash'
import { initializeApp } from 'firebase/app'
import { getAnalytics } from 'firebase/analytics'
import { getAuth, GoogleAuthProvider, signInWithPopup, onAuthStateChanged } from 'firebase/auth'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyBuPDZ4hBw_bY1EXY0vq0m_KlQHyCiUlBI',
  authDomain: 'inkheart-a4016.firebaseapp.com',
  projectId: 'inkheart-a4016',
  storageBucket: 'inkheart-a4016.appspot.com',
  messagingSenderId: '1043858130670',
  appId: '1:1043858130670:web:c1b1981cc04845939a55ff',
}

// Initialize Firebase

// ... Firebase Config ...
// Initialize Firebase
export const initFirebase = memoize(() => {
  const app = initializeApp(firebaseConfig)
  const auth = getAuth(app)
  return { app, auth, onAuthStateChanged, GoogleAuthProvider, signInWithPopup }
})
