interface LibraryItem {
  id: string
  filename: string
  title: string
  cover_name: string
  is_dir: boolean
  dir_previews: string[]
  children: LibraryItem[]
  last_modified: number
  created: number
}

export type { LibraryItem }
