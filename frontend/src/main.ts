import './theme.css'
import './fonts.css'
import './app.css'
import { getMeta } from './utils/meta'
import AppRouter from './AppRouter.svelte'
import { initTheme } from './lib/theme'

initTheme()

const metaTags = getMeta()

const useAuth = metaTags['inkheart:use-auth'] === '$INK:USEAUTH' ? false : metaTags['inkheart:use-auth'] === 'true'

const app = new AppRouter({
  target: document.getElementById('app'),
  props: { useAuth },
})

export default app
