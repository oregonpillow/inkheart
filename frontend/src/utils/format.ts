const formatPrecision = (num: number, precision: number) =>
  (Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision)).toFixed(precision)

export { formatPrecision }
