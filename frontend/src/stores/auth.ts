import { writable } from 'svelte/store'

type User = {
  uid: string
  email: string
}

const auth = writable<User | null>(null)
const usingAuth = writable<boolean>(false)

export { auth, usingAuth }
