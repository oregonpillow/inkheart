#!/bin/sh

# Check if required environment variables are set
if [ -z "$CI_REGISTRY_IMAGE" ] || [ -z "$CI_COMMIT_SHA" ]; then
    echo "Required environment variables CI_REGISTRY_IMAGE and CI_COMMIT_SHA are not set. This script should not be run outside of a gitlab pipeline. Exiting..."
    exit 1
fi

# Build cargo layer with cargo-chef and registry cache
docker buildx build --progress=plain $PWD -f docker/dockerfile \
  --target rust_builder \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/cargo:cache \
  --cache-to type=registry,ref=$CI_REGISTRY_IMAGE/cargo:cache,mode=max \
  --tag $CI_REGISTRY_IMAGE/cargo:latest \

# Build npm layer with registry cache
docker buildx build $PWD -f docker/dockerfile \
  --target node_builder \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/npm:cache \
  --cache-to type=registry,ref=$CI_REGISTRY_IMAGE/npm$:cache,mode=max \
  --tag $CI_REGISTRY_IMAGE/npm:latest \

# Final build with all cache layers
# Final build with intermediate cache layers only
docker buildx build $PWD -f docker/dockerfile \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/cargo:cache \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/npm:cache \
  --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA \
  --load

# Export build artifacts (using local output)
docker buildx build $PWD -f docker/dockerfile \
  --target export \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/cargo:cache \
  --cache-from type=registry,ref=$CI_REGISTRY_IMAGE/npm:cache \
  --output type=local,dest=build